import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { ImageUploadModule } from "angular2-image-upload";

import { AngularFireModule, FirebaseApp } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { firebaseConfig } from './../environments/environment'

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth/auth.service';
import { ProfilePopoverComponent } from './components/profile-popover/profile-popover.component';
import { ApplicantComponent } from './components/applicant/applicant.component';
import { JobsComponent } from './components/jobs/jobs.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AuthGuard } from './guards/auth.guard';

@NgModule({
    declarations: [
        AppComponent,
        ProfilePopoverComponent,
        ApplicantComponent,
        JobsComponent
    ],
    entryComponents: [
        ProfilePopoverComponent,
        ApplicantComponent,
        JobsComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot({
            mode: 'ios',
            backButtonText: 'Regresar',
        }),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        HttpModule,
        ImageUploadModule.forRoot(),
    ],
    providers: [
        StatusBar,
        SplashScreen,
        AuthGuard,
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        },
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
