import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  // { path: '**', redirectTo: 'landing'},
  { path: '', loadChildren: './pages/login/login.module#LoginPageModule', canActivate: [AuthGuard]},
  { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule', canActivate: [AuthGuard]},
  { path: 'empresa/:enterprise', loadChildren: './pages/enterprise/enterprise.module#EnterprisePageModule', canActivate: [AuthGuard]},
  { path: 'empresa/:enterprise/trabajos/:name/:key', loadChildren: './pages/job/job.module#JobPageModule', canActivate: [AuthGuard]},
  { path: 'personas', loadChildren: './pages/people/people.module#PeoplePageModule', canActivate: [AuthGuard]},
  { path: 'empresas', loadChildren: './pages/enterprises/enterprises.module#EnterprisesPageModule', canActivate: [AuthGuard]},
  { path: 'persona/:name/:key', loadChildren: './pages/person/person.module#PersonPageModule', canActivate: [AuthGuard]},
  { path: ':name/agrega_plaza', loadChildren: './pages/add-job/add-job.module#AddJobPageModule', canActivate: [AuthGuard]},
  { path: 'aplicantes/:enterprise/:key_job', loadChildren: './pages/applicants/applicants.module#ApplicantsPageModule', canActivate: [AuthGuard]},
  { path: 'plazas', loadChildren: './pages/jobs/jobs.module#JobsPageModule', canActivate: [AuthGuard]},
  { path: 'reportes', loadChildren: './pages/reports/reports.module#ReportsPageModule', canActivate: [AuthGuard]},
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'agregar_empresa', loadChildren: './pages/add-enterprise/add-enterprise.module#AddEnterprisePageModule' },
  { path: 'agregar_persona', loadChildren: './pages/add-person/add-person.module#AddPersonPageModule' },
  { path: 'actualizar_persona/:name/:key', loadChildren: './pages/edit-person/edit-person.module#EditPersonPageModule' },
  { path: 'actualizar_empresa/:name/:key', loadChildren: './pages/edit-enterprise/edit-enterprise.module#EditEnterprisePageModule' },
  { path: 'programas', loadChildren: './pages/programs/programs.module#ProgramsPageModule' },
  { path: 'usuarios', loadChildren: './pages/users/users.module#UsersPageModule' },
  // { path: 'all-jobs', loadChildren: './pages/all-jobs/all-jobs.module#AllJobsPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
