import { Injectable } from '@angular/core'
import { Router, CanActivate } from '@angular/router'
import { AuthService } from '../services/auth/auth.service';


@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private auth: AuthService
    ) {

    }

    canActivate(route) {
        // console.log(route);
        // return true;

        if (route.routeConfig.path == 'login') {
            if (this.auth.token) {
                this.router.navigate(['/dashboard']);
                return true;
            } else {
                return true;
            }
        } else {
            if (this.auth.token) {
                return true;
            } else {
                this.router.navigate(['/login'])
                return false
            }
        }
    }
}