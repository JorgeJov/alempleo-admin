import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

    @Input('user') user: any;
    rol: any;

    constructor(
        public auth: AuthService,
        public api: ApiService
    ) {
        this.api.getMember()
            .on('value', Snapshot => {
                if (Snapshot.val()) {
                    this.rol = Snapshot.val().rol;
                }
            });
    }

    ngOnInit() {

    }

}
