import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ProfilePopoverComponent } from '../profile-popover/profile-popover.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input('user') user: any;

  constructor(
    public popoverCtrl: PopoverController,
  ) {
  }

  ngOnInit() { }

  async presentProfilePopover(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: ProfilePopoverComponent,
      event: ev,
    });
    return await popover.present();
  }

}
