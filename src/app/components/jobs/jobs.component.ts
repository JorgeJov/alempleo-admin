import { Component, OnInit, NgZone } from '@angular/core';
import { NavParams, LoadingController, AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-jobs',
    templateUrl: './jobs.component.html',
    styleUrls: ['./jobs.component.scss'],
})
export class JobsComponent implements OnInit {


    applicant;
    jobs: any = [];
    jobsData: boolean = false;

    constructor(
        navParams: NavParams,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController
    ) {
        this.applicant = navParams.get('applicant');

        this.api.getJobs()
            .orderByChild('status')
            .equalTo('abierta')
            .on('value', Snapshots => {
                this.jobs = [];
                Snapshots.forEach(snapshot => {
                    let item = snapshot.val();
                    item.key = snapshot.key;

                    this.api.GetJobApplicants(item.key)
                        .orderByChild('applicant_key')
                        .equalTo(this.applicant.key)
                        .on('value', Snapshot => {
                            if (Snapshot.val()) {

                            } else {
                                this.jobs.push(item);
                                this.forceUpdate();
                            }
                        });
                    this.forceUpdate();
                });

                this.jobsData = true;
                this.forceUpdate();
            });

    }

    ngOnInit() { }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    async applyToAJob(job) {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.getApplications()
                    .orderByChild('applicant/applicant_key')
                    .equalTo(this.applicant.key)
                    .once('value', Snapshots => {

                        if (Snapshots.val()) {
                            let already = false;
                            let applicant_history;

                            Snapshots.forEach(element => {
                                let applicant_ = element.val();
                                if (applicant_.job.enterprise.key == job.enterprise.key) {
                                    applicant_history = element.val();
                                    already = true;
                                }
                            });

                            if (already) {
                                loader.dismiss();
                                this.alreadyApplied(applicant_history, this.applicant, job);
                            } else {
                                loader.dismiss();
                                this.applyNow(this.applicant, job);
                            }

                        } else {
                            loader.dismiss();
                            this.applyNow(this.applicant, job);
                        }
                    });
            });
        });
    }

    async alreadyApplied(applicant_history, applicant, job) {
        const alert = await this.alertController.create({
            header: `Este usuario ya ah aplicado a plazas con esta empresa`,
            message: `Este usuario ya aplico a la plaza de ${applicant_history.job.position} el dia ${applicant_history.date}`,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'cancelButtonAlert',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Aplicar de todas formas',
                    cssClass: 'confirmButtonAlert',
                    handler: () => {
                        this.api.applyToAJob(job, applicant);
                    }
                }
            ]
        });

        await alert.present();
    }

    async applyNow(applicant, job) {
        const alert = await this.alertController.create({
            header: ``,
            message: `Solo confirma que deseas enviar a ${applicant.name} a la plaza <strong>${job.position}</strong> de <strong>${job.enterprise.name}</strong>?`,
            cssClass: 'applyToJob',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'cancelButtonAlert',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Aplicar',
                    cssClass: 'confirmButtonAlert',
                    handler: () => {
                        this.api.applyToAJob(job, applicant);
                    }
                }
            ]
        });

        await alert.present();
    }

}
