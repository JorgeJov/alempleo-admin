import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-profile-popover',
    templateUrl: './profile-popover.component.html',
    styleUrls: ['./profile-popover.component.scss'],
})
export class ProfilePopoverComponent implements OnInit {

    constructor(
        private auth: AuthService
    ) { }

    ngOnInit() {

    }

    logout() {
        console.log('dando click');
        this.auth.logOut();
    }
}
