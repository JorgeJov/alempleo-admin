import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddPersonPage } from './add-person.page';
import { SharedComponentsModule } from 'src/app/components/shared-components.module';
import { ImageUploadModule } from "angular2-image-upload";

const routes: Routes = [
  {
    path: '',
    component: AddPersonPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ImageUploadModule.forRoot(),
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SharedComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddPersonPage]
})
export class AddPersonPageModule {}
