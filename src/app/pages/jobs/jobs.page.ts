import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-jobs',
    templateUrl: './jobs.page.html',
    styleUrls: ['./jobs.page.scss'],
})
export class JobsPage implements OnInit {

    jobsData: boolean = false;
    workItemsData: boolean = false;
    work_items: any = [];
    jobs: any = [];
    user: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        private auth: AuthService
    ) {


        this.getData();
        // this.auth.getApplicant(this.auth.token)
        //     .on('value', Snapshot => {
        //         console.log(Snapshot.val());

        //         // this.user = Snapshot.val();
        //         // this.user.key = Snapshot.key;
        //         // if (this.user) {
        //         //     loader.dismiss();
        //         //     this.getData();
        //         // } else {
        //         //     loader.dismiss();
        //         //     this.auth.logOut();
        //         // }
        //     });


    }

    ngOnInit() {

    }

    getData() {
        this.api.getJobs()
            .orderByChild('status')
            .equalTo('abierta')
            .on('value', Snapshots => {
                this.jobs = [];

                Snapshots.forEach(childSnapshot => {
                    let item = childSnapshot.val();
                    item.key = childSnapshot.key;
                    if (!item.tracing) {
                        this.api.getPicture(item.enterprise.key)
                            .on('value', Snapshot => {
                                item.enterprise.image = Snapshot.val();
                                if (item.enterprise.image == null) {
                                    item.enterprise.image = {
                                        downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                    }
                                }
                                this.jobs.push(item);
                                this.forceUpdate();
                            });
                    }
                });

                this.jobsData = true;
                this.forceUpdate();
            });

        // this.api.getWorkItems()
        //     .on('value', Snapshots => {
        //         this.work_items = [];
        //         this.work_items = this.snapshotToArray(Snapshots);
        //         this.workItemsData = true;
        //         this.forceUpdate();
        //     })
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

}
