import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { ApplicantComponent } from 'src/app/components/applicant/applicant.component';

@Component({
    selector: 'app-job',
    templateUrl: './job.page.html',
    styleUrls: ['./job.page.scss'],
})
export class JobPage implements OnInit {

    job_key: string;
    enterprise: any;
    jobsData: boolean = false;
    jobs: any = [];
    job: any;
    applicantsData: boolean = false;
    optionsData: boolean = false;
    options: any;
    applicants: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        public modalController: ModalController
    ) {

        this.job_key = this.route.snapshot.paramMap.get('key');
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.getJob(this.job_key)
                    .on('value', Snapshot => {
                        let job = Snapshot.val();
                        job.key = Snapshot.key;
                        this.api.getPicture(job.enterprise.key)
                            .on('value', Snapshot => {
                                job.enterprise.image = Snapshot.val();
                                if(job.enterprise.image == null){
                                    job.enterprise.image = {
                                        downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                    }
                                }
                                this.job = job;
                                this.forceUpdate();
                                loader.dismiss();
                                this.init();
                            })
                    });
            });
        });
    }

    ngOnInit() {
    }

    init() {
        this.api.GetJobApplicants(this.job_key)
            .on('value', result => {
                this.applicants = [];
                result.forEach(element => {
                    let applicant = element.val();
                    applicant.key = element.key;

                    this.api.getPicture(applicant.applicant_key)
                        .on('value', Snapshot => {
                            applicant.image = Snapshot.val();
                            if(applicant.image == null){
                                applicant.image = {
                                    downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                }
                            }
                            this.applicants.push(applicant);
                            this.forceUpdate();
                        })
                });
                this.applicantsData = true;
                this.forceUpdate();

                this.api.getApplicants()
                    .orderByChild(`work_items/${this.job.kind_job.key}/key`)
                    .equalTo(this.job.kind_job.key)
                    .on('value', result => {
                        this.options = [];
                        result.forEach(element => {
                            let applicant = element.val();
                            applicant.key = element.key;
                            if (this.search(applicant.key, this.applicants)) {

                            } else {
                                this.api.getPicture(applicant.key)
                                    .on('value', Snapshot => {
                                        applicant.image = Snapshot.val();
                                        if(applicant.image == null){
                                            applicant.image = {
                                                downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                            }
                                        }
                                        this.options.push(applicant);
                                        this.forceUpdate();
                                    })
                            }
                        });
                        this.optionsData = true;
                        this.forceUpdate();
                    })

            })
    }

    snapshotToArray = snapshot => {
        let returnArr = [];
        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });
        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    async previewApplicant(applicant) {

        const modal = await this.modalController.create({
            component: ApplicantComponent,
            componentProps: {
                applicant: applicant,
                job: this.job
            }
        });
        return await modal.present();
    }

    async applyToAJob(applicant) {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.getApplications()
                    .orderByChild('applicant/applicant_key')
                    .equalTo(applicant.key)
                    .once('value', Snapshots => {
                        
                        if(Snapshots.val()){
                            let already = false;
                            let applicant_history;

                            Snapshots.forEach(element => {
                                let applicant_ = element.val();
                                if (applicant_.job.enterprise.key == this.job.enterprise.key) {
                                    applicant_history = element.val();
                                    already = true;
                                }
                            });

                            if (already) {
                                loader.dismiss();
                                this.alreadyApplied(applicant_history, applicant);
                            } else {
                                loader.dismiss();
                                this.applyNow(applicant);
                            }

                        }else{
                            loader.dismiss();
                            this.applyNow(applicant);
                        }
                    });
            });
        });
    }

    async alreadyApplied(applicant_history, applicant) {
        const alert = await this.alertController.create({
            header: `Este usuario ya ah aplicado a plazas con esta empresa`,
            message: `Este usuario ya aplico a la plaza de ${applicant_history.job.position} el dia ${applicant_history.date}`,
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'cancelButtonAlert',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Aplicar de todas formas',
                    cssClass: 'confirmButtonAlert',
                    handler: () => {
                        this.api.applyToAJob(this.job, applicant);
                    }
                }
            ]
        });

        await alert.present();
    }

    async applyNow(applicant) {
        const alert = await this.alertController.create({
            header: ``,
            message: `Solo confirma que deseas enviar a ${applicant.name} a la plaza <strong>${this.job.position}</strong> de <strong>${this.job.enterprise.name}</strong>?`,
            cssClass: 'applyToJob',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'cancelButtonAlert',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Aplicar',
                    cssClass: 'confirmButtonAlert',
                    handler: () => {
                        this.api.applyToAJob(this.job, applicant);
                    }
                }
            ]
        });

        await alert.present();
    }

    search(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].applicant_key === nameKey) {
                return myArray[i];
            }
        }
    }

}
