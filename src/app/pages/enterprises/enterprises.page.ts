import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-enterprises',
    templateUrl: './enterprises.page.html',
    styleUrls: ['./enterprises.page.scss'],
})
export class EnterprisesPage implements OnInit {

    enterprisesData;
    enterprises: any;
    enterprises_full: any;

    constructor(
        public api: ApiService,
        private zone: NgZone
    ) {
        this.api.getEnterprises()
            .orderByChild('status')
            .on('value', Snapshots => {
                this.enterprises = [];
                this.enterprises_full = [];
                Snapshots.forEach(element => {
                    let item = element.val();
                    item.key = element.key;
                    item.name_url = item.name.replace(" ", "_");
                    this.api.getPicture(item.key)
                        .on('value', Snapshot => {
                            item.image = Snapshot.val();
                            
                            if (item.image == null) {
                                item.image = {
                                    downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                }
                                console.log(item.image);
                                
                            }
                            item.plazas_activas = Snapshots.numChildren();
                            item.searchQ = `${item.name} ${item.email} ${item.description} ${item.phone}`;
                            this.enterprises_full.push(item)
                            this.enterprises.push(item)
                            this.forceUpdate();
                        })
                });
                this.enterprisesData = true;
                this.forceUpdate();
            });
    }

    ngOnInit() {
    }

    segmentChanged(event) {

    }

    setSearch(ev) {
        let searchTerm = ev.target.value;
        if (searchTerm == '') {
            this.enterprises = this.enterprises_full;
        } else {
            this.enterprises = this.enterprises_full.filter((item) => {
                return item.searchQ.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
        }
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

}
