import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterprisesPage } from './enterprises.page';

describe('EnterprisesPage', () => {
  let component: EnterprisesPage;
  let fixture: ComponentFixture<EnterprisesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterprisesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterprisesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
