import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    loginForm: FormGroup;

    constructor(
        public formBuilder: FormBuilder,
        private auth: AuthService,
        public alertCtrl: AlertController,
        private router: Router,
        public loadingCtrl: LoadingController
    ) {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]]
        });
    }

    ngOnInit() {
        console.log('Login Page');
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    login() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.auth.login(this.loginForm.value.email, this.loginForm.value.password).then(data => {
                    loader.dismiss();
                    this.router.navigate(['/dashboard'])
                }, err => {
                    this.alertCtrl.create({
                        header: '¡Ups!',
                        // subHeader: 'Subtitle',
                        message: 'Hay un error con tus correo o contraseña, intentalo nuevamente',
                        buttons: ['Aceptar']
                    }).then(alert => {
                        loader.dismiss();
                        alert.present();
                    })
                });
            });
        });
    }
}
