import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from 'src/app/services/api/api.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
    selector: 'app-add-job',
    templateUrl: './add-job.page.html',
    styleUrls: ['./add-job.page.scss'],
})
export class AddJobPage implements OnInit {

    user: any;
    enterprise: any;
    work_items: any;
    aptitudes: any = [];
    addWorkForm: FormGroup;
    enterpriseName: any;

    constructor(
        private auth: AuthService,
        public loadingCtrl: LoadingController,
        private route: ActivatedRoute,
        public router: Router,
        public api: ApiService,
        public alertCtrl: AlertController,
        public formBuilder: FormBuilder,
        public alertController: AlertController
    ) {
        this.enterpriseName = this.route.snapshot.paramMap.get('name');
        this.enterpriseName = this.enterpriseName.replace("_", " ")

        this.addWorkForm = this.formBuilder.group({
            position: ['', [Validators.required]],
            salary: ['', [Validators.required]],
            branch: ['', [Validators.required]],
            type: ['', [Validators.required]],
            experience: ['', [Validators.required]],
            kind_job: ['', [Validators.required]],
            gender: ['', [Validators.required]],
            description: ['', [Validators.required]],
            number: ['', [Validators.required]],
            expiration: ['', [Validators.required]],
            aptitudes: [''],
        });



        this.loader().then(loader => {
            loader.present().then(() => {

                this.api.getEnterprises()
                    .orderByChild('name')
                    .equalTo(this.enterpriseName)
                    .limitToFirst(1)
                    .on('value', resp => {
                        resp.forEach(element => {
                            let enterprise = element.val();
                            enterprise.key = element.key;
                            enterprise.name_url = enterprise.name.replace(" ", "_");
                            this.enterprise = enterprise;
                        });
                        loader.dismiss();
                    });
            });
        });
    }

    ngOnInit() {
        this.api.getWorkItems()
            .on('value', Snapshots => {
                this.work_items = [];
                Snapshots.forEach(element => {
                    let work_item = element.val();
                    work_item.key = element.key;
                    this.work_items.push(work_item);
                });
            })
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    async addAptitud() {
        const alert = await this.alertCtrl.create({
            header: 'Agrega aptitud',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Aptitud'
                },

            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: data => {
                        this.aptitudes.push(data);
                        this.addWorkForm.controls['aptitudes'].setValue(this.aptitudes);
                    }
                }
            ]
        });

        await alert.present();
    }

    removeAptitud(aptitud, index) {
        this.aptitudes.splice(index, 1);
        if (this.isEmpty(this.aptitudes)) {
            this.addWorkForm.controls['aptitudes'].setValue('');
        } else {
            this.addWorkForm.controls['aptitudes'].setValue(this.aptitudes);
        }
    }

    isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }


    addWork() {
        this.loader().then(loader => {
            loader.present().then(() => {
                let data = this.addWorkForm.value;
                data.enterprise = this.enterprise;
                this.api.addJob(this.addWorkForm.value, this.enterprise).then(data => {
                    loader.dismiss();
                    this.presentAlert('Plaza agregada exitosamente', 'Felicidades tu plaza ah sido agregada correctamente');
                }, err => {
                    loader.dismiss();
                });
            });
        });
    }

    async presentAlert(title, message) {
        const alert = await this.alertController.create({
            header: title,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
    }

}
