import { Component, OnInit, NgZone } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
    selector: 'app-programs',
    templateUrl: './programs.page.html',
    styleUrls: ['./programs.page.scss'],
})
export class ProgramsPage implements OnInit {

    programs: any;
    constructor(
        public alertController: AlertController,
        public api: ApiService,
        private zone: NgZone
    ) {
        this.api.getPrograms()
            .on('value', snapshots => {
                this.programs = [];
                snapshots.forEach(element => {
                    let program = element.val();
                    program.key = element.key;
                    this.programs.push(program);
                });
                console.log(this.programs);

                this.forceUpdate();
            });
    }

    ngOnInit() {
    }

    async addProgram() {
        const alert = await this.alertController.create({
            header: 'Agregar un programa',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre del programa'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.api.addProgram(data).then(data => {
                            console.log(data);
                        })
                    }
                }
            ]
        });

        await alert.present();
    }
    removeProgram(program) {
        this.api.removeProgram(program.key).then(data=>{
            console.log('donde');
        })
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

}
