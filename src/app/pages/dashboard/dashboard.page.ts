import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

    applicantsData;
    enterprisesData;
    jobsData: boolean = false;
    jobsFollowinData: boolean = false;

    applicants: any = [];
    enterprises: any;
    jobs: any;
    jobs_following: any;

    constructor(
        public api: ApiService,
        private zone: NgZone,
        public auth: AuthService
    ) {
        this.api.getApplicants()
            .orderByChild('status')
            .equalTo('pending_authorization')
            .limitToLast(5)
            .on('value', Snapshots => {
                this.applicants = [];
                Snapshots.forEach(childSnapshot => {
                    let item = childSnapshot.val();
                    item.key = childSnapshot.key;
                    item.name_url = item.name.replace(" ", "_");
                    item.name_url += '_' + item.lastname.replace(" ", "_");
                    this.api.getPicture(item.key)
                        .on('value', Snapshot => {
                            item.image = Snapshot.val();
                            if (item.image == null) {
                                item.image = {
                                    downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                }
                            }
                            this.applicants.push(item);
                        })
                });
                this.applicantsData = true;
                this.forceUpdate();
            });

        this.api.getEnterprises()
            .orderByChild('status')
            .equalTo('pending_authorization')
            .limitToLast(5)
            .on('value', Snapshots => {
                this.enterprises = [];
                Snapshots.forEach(childSnapshot => {
                    let item = childSnapshot.val();
                    item.key = childSnapshot.key;
                    item.name_url = item.name.replace(" ", "_");
                    this.api.getPicture(item.key)
                        .on('value', Snapshot => {
                            item.image = Snapshot.val();
                            if (item.image == null) {
                                item.image = {
                                    downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                }
                            }
                            this.enterprises.push(item);
                        })
                });
                this.enterprisesData = true;
                this.forceUpdate();
            });

        this.api.getJobs()
            .limitToFirst(5)
            .orderByChild('status')
            .equalTo('abierta')
            .on('value', snapshots => {
                this.jobs = [];
                snapshots.forEach(childSnapshot => {
                    let item = childSnapshot.val();
                    item.key = childSnapshot.key;
                    item.name_url = item.position.replace(" ", "_");

                    this.api.getPicture(item.enterprise.key)
                        .on('value', Snapshot => {
                            item.enterprise.image = Snapshot.val();
                            if (item.enterprise.image == null) {
                                item.enterprise.image = {
                                    downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                }
                            }

                            this.jobs.push(item);
                            this.forceUpdate();
                        })
                });

                this.jobsData = true;
                this.forceUpdate();
            });

        this.api.getJobs()
            .orderByChild('status')
            .equalTo('abierta')
            .on('value', snapshots => {
                this.jobs_following = [];
                snapshots.forEach(Snapshots => {
                    let item = Snapshots.val();
                    item.key = Snapshots.key;
                    if (item.tracing == this.auth.token) {
                        item.name_url = item.position.replace(" ", "_");

                        this.api.GetJobApplicants(item.key)
                            .on('value', result => {
                                item.applicants = [];
                                result.forEach(element => {
                                    let applicant = element.val();
                                    applicant.key = element.key;
                                    this.api.getPicture(applicant.applicant_key)
                                        .on('value', Snapshot => {
                                            applicant.image = Snapshot.val();
                                            if (applicant.image == null) {
                                                applicant.image = {
                                                    downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                                }
                                            }
                                            item.applicants.push(applicant);
                                            this.forceUpdate();
                                        })
                                });
                                console.log(item.applicants);
                                
                                
                                this.applicantsData = true;
                                this.forceUpdate();
                            })

                        this.jobs_following.push(item);
                        this.forceUpdate();
                    }
                });
                this.jobsFollowinData = true;
                this.forceUpdate();
            });
    }

    ngOnInit() {

    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

}
