import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

@Component({
    selector: 'app-people',
    templateUrl: './people.page.html',
    styleUrls: ['./people.page.scss'],
})
export class PeoplePage implements OnInit {

    peopleData;
    people: any;
    people_full: any;

    searching: any = false;
    searchTerm: string = '';

    constructor(
        public api: ApiService,
        private zone: NgZone
    ) {

        this.api.getPeople()
            .on('value', Snapshots => {
                this.people = [];
                this.people_full = [];
                Snapshots.forEach(element => {
                    let item = element.val();
                    item.key = element.key;
                    item.name_url = item.name.replace(" ", "_");
                    item.name_url += '_' + item.lastname.replace(" ", "_");
                    
                    
                    if(item.birthdate !== undefined){
                        item.birthdate = this.calculateAge(new Date(item.birthdate));
                    }
                    this.api.getPicture(item.key)
                        .on('value', Snapshot => {
                            item.image = Snapshot.val();
                            if (item.image == null) {
                                item.image = {
                                    downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                }
                            }
                            item.searchQ = `${item.name} ${item.lastname} ${item.email} ${item.salary} ${item.status} ${item.experience} ${item.aptitudes} `;
                            this.people_full.push(item)
                            this.people.push(item)
                            this.forceUpdate();
                        })
                });
                console.log(this.people);

                this.peopleData = true;
                this.forceUpdate();
            });
    }

    ngOnInit() {
    }

    calculateAge(birthday) {
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    setSearch(ev) {
        let searchTerm = ev.target.value;
        if (searchTerm == '') {
            this.people = this.people_full;
        } else {
            this.people = this.people_full.filter((item) => {
                return item.searchQ.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
        }
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

}
