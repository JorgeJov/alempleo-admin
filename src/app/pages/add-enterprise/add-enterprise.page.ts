import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
    selector: 'app-add-enterprise',
    templateUrl: './add-enterprise.page.html',
    styleUrls: ['./add-enterprise.page.scss'],
})
export class AddEnterprisePage implements OnInit {

    enterpriseForm: FormGroup;
    branches: any = [];
    constructor(
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        private route: ActivatedRoute,
        public router: Router,
        public alertCtrl: AlertController,
        public formBuilder: FormBuilder,
    ) {
        this.enterpriseForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            lastname: ['', [Validators.required]],
            enterprise: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            image: [''],
            email_contact: [''],
            phone_contact: [''],
            branches: [''],
        });
    }

    ngOnInit() {
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    createEnterprise() {
        this.loader().then(loader => {
            loader.present().then(() => {
                let data = this.enterpriseForm.value;
                this.api.createEnterprise(data).then(data => {
                    loader.dismiss();
                    this.presentAlert('Empresa agregada exitosamente', 'Un correo electornico ah sido enviado al encargado de la empresa');
                }, err => {
                    loader.dismiss();
                });
            });
        });
    }

    async presentAlert(title, message) {
        const alert = await this.alertCtrl.create({
            header: title,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
    }

    async addBranch() {
        const alert = await this.alertCtrl.create({
            header: 'Agrega una sucursal',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre de sucursal'
                },
                {
                    name: 'address',
                    type: 'text',
                    placeholder: 'Dirección'
                },
                {
                    name: 'number',
                    type: 'number',
                    placeholder: 'Numero telefonico'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: data => {
                        this.branches.push(data);
                        this.enterpriseForm.controls['branches'].setValue(this.branches);
                    }
                }
            ]
        });

        await alert.present();
    }

    RemoveBranch(branch, index) {
        this.branches.splice(index, 1);
        if (this.isEmpty(this.branches)) {
            this.enterpriseForm.controls['branches'].setValue('');
        } else {
            this.enterpriseForm.controls['branches'].setValue(this.branches);
        }
    }

    isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    onUploadFinished(file) {
        this.enterpriseForm.controls['image'].setValue(file.src);
    }

}
