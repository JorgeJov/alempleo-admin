import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddEnterprisePage } from './add-enterprise.page';
import { SharedComponentsModule } from 'src/app/components/shared-components.module';
import { ImageUploadModule } from 'angular2-image-upload';

const routes: Routes = [
  {
    path: '',
    component: AddEnterprisePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SharedComponentsModule,
    ImageUploadModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [AddEnterprisePage]
})
export class AddEnterprisePageModule {}
