import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-enterprise',
    templateUrl: './enterprise.page.html',
    styleUrls: ['./enterprise.page.scss'],
})
export class EnterprisePage implements OnInit {

    enterpriseName: string;
    enterprise: any;
    jobsData: boolean = false;
    jobs: any = [];

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        public auth: AuthService
    ) {
        this.enterpriseName = this.route.snapshot.paramMap.get('enterprise');
        this.enterpriseName = this.enterpriseName.replace("_", " ")

        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.getEnterprises()
                    .orderByChild('name')
                    .equalTo(this.enterpriseName)
                    .limitToFirst(1)
                    .on('value', resp => {
                        resp.forEach(element => {
                            let enterprise = element.val();
                            enterprise.key = element.key;
                            enterprise.name_url = enterprise.name.replace(" ", "_");

                            this.api.getPicture(enterprise.key)
                                .on('value', Snapshot => {
                                    enterprise.image = Snapshot.val();
                                    if (enterprise.image == null) {
                                        enterprise.image = {
                                            downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                        }
                                    }
                                    this.enterprise = enterprise;
                                    console.log(enterprise);
                                    
                                    loader.dismiss();
                                    this.forceUpdate();
                                    this.getAllData();
                                })
                        });
                    });
            });
        });
    }

    ngOnInit() {

    }

    getAllData() {
        this.api.getJobs()
            .orderByChild('enterprise/key')
            .equalTo(this.enterprise.key)
            .on('value', Snapshots => {
                this.jobs = [];
                Snapshots.forEach(childSnapshot => {
                    let item = childSnapshot.val();
                    if (item.status == 'abierta') {
                        item.key = childSnapshot.key;
                        item.name_url = item.position.replace(" ", "_");
                        this.jobs.push(item);
                    }
                });

                this.jobsData = true;
                this.forceUpdate();
            });
    }

    snapshotToArray = snapshot => {
        let returnArr = [];

        snapshot.forEach(childSnapshot => {
            let item = childSnapshot.val();
            item.key = childSnapshot.key;
            returnArr.push(item);
        });

        return returnArr;
    };

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    activateAccount() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.activeEnterprise(this.enterprise.key).then(data => {
                    loader.dismiss();
                    this.presentAlert('Cuenta activada correctamente', `La cuenta ${this.enterprise.name} ah sido activada correctamente`);
                })
            })
        });
    }


    async presentAlert(title, message) {
        const alert = await this.alertController.create({
            header: title,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
    }

    resetPassword() {
        this.api.get('user-enterprises')
            .orderByChild('enterprise/key')
            .equalTo(this.enterprise.key)
            .limitToFirst(1)
            .once('value', snapshot => {
                snapshot.forEach(element => {
                    console.log(element.val());
                    this.auth.resetPassword(element.val().email).then(data => {
                        this.presentAlert('Restauración de contraseña enviada', '');
                    });
                });
            })
    }


}
