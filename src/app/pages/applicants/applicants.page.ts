import { Component, OnInit, NgZone } from '@angular/core';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { ActivatedRoute } from '@angular/router';
import { ApplicantComponent } from 'src/app/components/applicant/applicant.component';

@Component({
    selector: 'app-applicants',
    templateUrl: './applicants.page.html',
    styleUrls: ['./applicants.page.scss'],
})
export class ApplicantsPage implements OnInit {

    job_key: string;
    applicantsData: boolean = false;
    optionsData: boolean = false;
    options: any;
    job: any;
    applicants: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        public modalController: ModalController
    ) {
        this.job_key = this.route.snapshot.paramMap.get('key_job');

        this.loader().then(loader => {
            loader.present().then(() => {

                this.api.GetJobApplicants(this.job_key)
                    .on('value', result => {
                        this.applicants = [];
                        result.forEach(element => {
                            let applicant = element.val();
                            applicant.key = element.key;
                            this.applicants.push(applicant);
                        });
                        this.applicantsData = true;
                        this.forceUpdate();

                        this.api.getJob(this.job_key)
                            .on('value', Snapshot => {
                                let job = Snapshot.val();
                                job.key = Snapshot.key;
                                this.job = job;

                                this.api.getApplicants()
                                    .orderByChild(`work_items/${this.job.kind_job.key}/key`)
                                    .equalTo(this.job.kind_job.key)
                                    .on('value', result => {
                                        this.options = [];
                                        result.forEach(element => {
                                            let applicant = element.val();
                                            applicant.key = element.key;
                                        
                                            if(this.search(applicant.key, this.applicants)){
                                                
                                            }else{
                                                this.options.push(applicant);
                                            }
                                        });
                                        this.optionsData = true;
                                        this.forceUpdate();
                                        loader.dismiss();
                                    })
                            });
                    })
            });
        });


    }

    ngOnInit() {
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    async previewApplicant(applicant) {

        const modal = await this.modalController.create({
            component: ApplicantComponent,
            componentProps: {
                applicant: applicant,
                job: this.job
            }
        });
        return await modal.present();
    }

    async applyToAJob(applicant) {

        const alert = await this.alertController.create({
            header: ``,
            message: `Solo confirma que deseas enviar a ${applicant.name} a la plaza <strong>${this.job.position}</strong> de <strong>${this.job.enterprise.name}</strong>?`,
            cssClass: 'applyToJob',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'cancelButtonAlert',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Aplicar',
                    cssClass: 'confirmButtonAlert',
                    handler: () => {
                        this.api.applyToAJob(this.job, applicant);
                    }
                }
            ]
        });

        await alert.present();

    }

    search(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].applicant_key === nameKey) {
                return myArray[i];
            }
        }
    }

}
