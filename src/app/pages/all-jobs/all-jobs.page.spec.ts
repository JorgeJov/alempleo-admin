import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllJobsPage } from './all-jobs.page';

describe('AllJobsPage', () => {
  let component: AllJobsPage;
  let fixture: ComponentFixture<AllJobsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllJobsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllJobsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
