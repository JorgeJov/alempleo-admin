import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
    selector: 'app-edit-enterprise',
    templateUrl: './edit-enterprise.page.html',
    styleUrls: ['./edit-enterprise.page.scss'],
})
export class EditEnterprisePage implements OnInit {

    enterpriseForm: FormGroup;
    branches: any = [];
    enterprise: any;
    enterprise_key: any;
    image_enterprise: any;

    constructor(
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        private route: ActivatedRoute,
        public router: Router,
        public alertCtrl: AlertController,
        public formBuilder: FormBuilder,
    ) {
        this.enterpriseForm = this.formBuilder.group({
            enterprise: ['', [Validators.required]],
            image: [''],
            name_contact: [''],
            lastname_contact: [''],
            email_contact: [''],
            phone_contact: [''],
            branches: [''],
        });
        this.enterprise_key = this.route.snapshot.paramMap.get('key');
        this.api.getEnterprise(this.enterprise_key)
            .on('value', Snapshots => {
                this.enterprise = Snapshots.val();

                this.api.getPicture(this.enterprise.key)
                    .on('value', Snapshot => {
                        this.image_enterprise = Snapshot.val();
                        if (this.image_enterprise == null) {
                            this.image_enterprise = {
                                downloadURL: 'http://www.alempleo.org/demo/img/imageholder.jpg'
                            }
                        }
                    });
                if (this.enterprise.branches) {
                    this.branches = this.enterprise.branches;
                }
                this.enterpriseForm = this.formBuilder.group({
                    enterprise: [this.enterprise.name, [Validators.required]],
                    name_contact: [this.enterprise.name_contact],
                    lastname_contact: [this.enterprise.lastname_contact],
                    image: [''],
                    email_contact: [this.enterprise.email_contact],
                    phone_contact: [this.enterprise.phone_contact],
                    branches: [this.enterprise.branches],
                });
            });
    }

    ngOnInit() {
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    updateEnterprise() {
        this.loader().then(loader => {
            loader.present().then(() => {
                let data = this.enterpriseForm.value;
                this.api.updateEnterprise(data, this.enterprise_key).then(data => {
                    loader.dismiss();
                    this.presentAlert('Empresa agregada exitosamente', 'Un correo electornico ah sido enviado al encargado de la empresa');
                }, err => {
                    loader.dismiss();
                });
            });
        });
    }

    async presentAlert(title, message) {
        const alert = await this.alertCtrl.create({
            header: title,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
    }

    async addBranch() {
        const alert = await this.alertCtrl.create({
            header: 'Agrega una sucursal',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre de sucursal'
                },
                {
                    name: 'address',
                    type: 'text',
                    placeholder: 'Dirección'
                },
                {
                    name: 'number',
                    type: 'number',
                    placeholder: 'Numero telefonico'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: data => {
                        this.branches.push(data);
                        this.enterpriseForm.controls['branches'].setValue(this.branches);
                    }
                }
            ]
        });

        await alert.present();
    }

    RemoveBranch(branch, index) {
        this.branches.splice(index, 1);
        if (this.isEmpty(this.branches)) {
            this.enterpriseForm.controls['branches'].setValue('');
        } else {
            this.enterpriseForm.controls['branches'].setValue(this.branches);
        }
    }

    isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    onUploadFinished(file) {
        this.enterpriseForm.controls['image'].setValue(file.src);
    }

}
