import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { JobsComponent } from 'src/app/components/jobs/jobs.component';
import * as jsPDF from 'jspdf'
import { Observable, Observer } from 'rxjs';

@Component({
    selector: 'app-person',
    templateUrl: './person.page.html',
    styleUrls: ['./person.page.scss'],
})
export class PersonPage implements OnInit {

    personName: string;
    person: any;
    jobsData: boolean = false;
    person_key: any;
    jobs: any = [];
    interests = [];

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        public modalController: ModalController
    ) {
        this.personName = this.route.snapshot.paramMap.get('name');
        this.personName = this.personName.replace("_", " ")
        this.person_key = this.route.snapshot.paramMap.get('key');

        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.getApplicant(this.person_key)
                    .orderByChild('name')
                    .on('value', resp => {
                        let person = resp.val();
                        person.key = resp.key;
                        if (person.birthdate) {
                            person.birthdate = this.calculateAge(new Date(person.birthdate));
                        }
                        for (var prop in person.interests) {
                            this.interests.push(person.interests[prop]);
                        }
                        this.api.getPicture(person.key)
                            .on('value', Snapshot => {

                                person.image = Snapshot.val();


                                if (person.image == null) {
                                    person.image = {
                                        downloadURL: 'http://www.alempleo.org/img/imageholder.jpg'
                                    }
                                }
                                this.person = person;
                                console.log(person);

                                loader.dismiss();
                                this.forceUpdate();
                            })
                    });
            });
        });
    }

    calculateAge(birthday) {
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    ngOnInit() {
        // for testing you can add any image url here or get dynamically  from other methods as you require


    }

    activateAccount() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.activeApplicants(this.person.key).then(data => {
                    loader.dismiss();
                    this.presentAlert('Cuenta activada correctamente', `La cuenta de  ${this.person.name} ah sido activada correctamente`);
                })
            })
        });
    }

    downloadCV() {
        this.api.getApplicant(this.person_key)
            .once('value', Snapshot => {
                window.open(Snapshot.val().cv, "_blank")
            })
    }

    async addToaJob() {
        const modal = await this.modalController.create({
            component: JobsComponent,
            componentProps: { applicant: this.person }
        });
        return await modal.present();
    }

    async presentAlert(title, message) {
        const alert = await this.alertController.create({
            header: title,
            message: message,
            buttons: ['OK']
        });

        await alert.present();
    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
            // message: 'Please wait...',
        });
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }


    getBase64ImageFromURL(url: string) {
        return Observable.create((observer: Observer<string>) => {
            let img = new Image();
            img.crossOrigin = 'Anonymous';
            img.src = url;


            if (!img.complete) {
                img.onload = () => {
                    observer.next(this.getBase64Image(img));
                    observer.complete();
                };
                img.onerror = (err) => {
                    observer.error(err);
                };
            } else {
                observer.next(this.getBase64Image(img));
                observer.complete();
            }
        });
    }

    getBase64Image(img: HTMLImageElement) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    }

    downloadProfile() {
        let position_height = 0;
        var options = {
            pagesplit: true
        };
        var doc = new jsPDF(options)
        let height = doc.internal.pageSize.height;

        this.getBase64ImageFromURL(this.person.image.downloadURL).subscribe(base64data => {

            doc.addImage('data:image/jpg;base64,' + base64data, 'JPEG', 160, 10, 40, 40);
            doc.setFontSize(18);
            doc.text(`${this.person.name} ${this.person.lastname}`, 20, position_height += 30)
            doc.setFontSize(12);
            // doc.image(`${this.getBase64Image(this.person.image.downloadURL)}`, 'JPEG', 20, position_height += 20 );
            doc.text(`Correo electronico: ${this.person.email}`, 20, position_height += 5)
            doc.text(`Estado Civil: ${this.person.civil_status}`, 20, position_height += 5)
            doc.text(`DUI: ${this.person.dui}`, 20, position_height += 5)
            doc.text(`NIT: ${this.person.nit}`, 20, position_height += 5)
            doc.text(`NUP (AFP): ${this.person.nup}`, 20, position_height += 5)
            doc.text(`Dirección: ${this.person.address}`, 20, position_height += 5)

            if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
            if (this.person.experience) {
                doc.setFontSize(18);
                doc.text(`Experiencia Laboral`, 20, position_height += 10)
                if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }

                this.person.experience.forEach(element => {
                    doc.setFontSize(14);
                    doc.setFontStyle("bold");
                    doc.text(`${element.name}`, 40, position_height += 10)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.setFontStyle("normal");
                    doc.setFontSize(12);
                    doc.text(`Telefono: ${element.phone}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.text(`Cargo desempeñado: ${element.position}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.text(`Principales funciones realizadas: ${element.main_functions}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.text(`Jefe inmediato: ${element.chief} Posición: ${element.chief_position}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.text(`Perioso: ${element.start}-${element.end}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                });
            }

            if (this.person.careers) {
                doc.setFontSize(18);
                doc.text(`Estudio Superiores`, 20, position_height += 10)
                if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                this.person.careers.forEach(element => {
                    doc.setFontSize(14);
                    doc.setFontStyle("bold");
                    doc.text(`${element.institution}`, 40, position_height += 10)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.setFontStyle("normal");
                    doc.setFontSize(12);
                    doc.text(`Nivel alcanzado: ${element.level}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.text(`Periodo: ${element.period}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                });
            }

            if (this.person.studys) {
                doc.setFontSize(18);
                doc.text(`Nivel de estudios alcanzado`, 20, position_height += 10)
                if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                this.person.studys.forEach(element => {
                    doc.setFontSize(14);
                    doc.setFontStyle("bold");
                    doc.text(`${element.institution}`, 40, position_height += 10)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.setFontStyle("normal");
                    doc.setFontSize(12);
                    doc.text(`Periodo: ${element.period}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                });
            }

            if (this.person.courses) {
                doc.setFontSize(18);
                doc.text(`Cursos o seminarios`, 20, position_height += 10)
                if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                this.person.courses.forEach(element => {
                    doc.setFontSize(14);
                    doc.setFontStyle("bold");
                    doc.text(`${element.name}`, 40, position_height += 10)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.setFontStyle("normal");
                    doc.setFontSize(12);
                    doc.text(`Institución: ${element.institution}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.text(`Duración: ${element.duration}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.text(`Fecha: ${element.date}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                });
            }

            if (this.person.references) {
                doc.setFontSize(18);
                doc.text(`Referencia Personales`, 20, position_height += 10)
                if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                this.person.references.forEach(element => {
                    doc.setFontSize(14);
                    doc.setFontStyle("bold");
                    doc.text(`${element.name}`, 40, position_height += 10)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.setFontStyle("normal");
                    doc.setFontSize(12);
                    doc.text(`Telefono: ${element.phone}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.text(`Empresa: ${element.enterprise}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                    doc.text(`Posición: ${element.position}`, 40, position_height += 5)
                    if (position_height + 30 >= height) { doc.addPage(); position_height = 20; console.log('deberia de agregar'); }
                });
            }
            doc.save(`${this.person.name} ${this.person.lastname}.pdf`)

        });







        // doc.save(`${this.person.name} ${this.person.lastname}.pdf`)
    }



}
