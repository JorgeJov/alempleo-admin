import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { AlertController } from '@ionic/angular';
import { database } from 'firebase';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ThrowStmt } from '@angular/compiler';
import { Router } from '@angular/router';

@Component({
    selector: 'app-users',
    templateUrl: './users.page.html',
    styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

    members: any;

    constructor(
        public api: ApiService,
        public alertController: AlertController,
        public auth: AuthService,
        private zone: NgZone,
        private router: Router
    ) {
        this.api.getMember()
            .on('value', Snapshot => {
                if (Snapshot.val().rol == 'supervisor') {
                    this.router.navigate(['/dashboard'])
                } else {
                    this.init();
                }
            });
    }

    init() {
        this.api.getMembers()
            .on('value', Snapshtos => {
                this.members = [];
                Snapshtos.forEach(element => {
                    let member = element.val();
                    member.key = element.key;
                    this.members.push(member);
                });
                this.forceUpdate();
            })
    }

    ngOnInit() {
    }

    async changeSettings(user) {
        const alert = await this.alertController.create({
            header: 'Cambia el tipo de usuario de ' + user.name,
            inputs: [
                {
                    name: 'admin',
                    type: 'radio',
                    label: 'Administrador',
                    value: 'admin'
                },
                {
                    name: 'supervisor',
                    type: 'radio',
                    label: 'Supervisor',
                    value: 'supervisor'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Ok',
                    handler: (data) => {
                        this.api.updateAdmin(user.key, data).then(data => {
                            console.log('functiono');
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    async addMember() {

        const alert = await this.alertController.create({
            header: 'Agregar nuevo usuario',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre'
                },
                {
                    name: 'email',
                    type: 'email',
                    placeholder: 'Correo electronico'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Continuar',
                    handler: (data) => {
                        this.secondStep(data);
                    }
                }
            ]
        });

        await alert.present();

    }

    async secondStep(data1) {

        const alert = await this.alertController.create({
            header: 'Tipo de usuario para ' + data1.name,
            inputs: [
                {
                    name: 'admin',
                    type: 'radio',
                    label: 'Administrador',
                    value: 'admin'
                },
                {
                    name: 'supervisor',
                    type: 'radio',
                    label: 'Supervisor',
                    value: 'supervisor'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Continuar',
                    handler: (data) => {
                        data1.rol = data;
                        this.auth.addAdmin(data1)
                    }
                }
            ]
        });

        await alert.present();

    }



    // dataent = []
    // addToDb() {

    //     this.dataent.forEach(element => {
    //         if (element.email) {
    //             let enterprise = {
    //                 alempleo: true,
    //                 email: element.email,
    //                 status: 'pending_authorization'
    //             };
    //             let user_enterprise = {
    //                 alempleo: true,
    //                 email: element.email,
    //                 type: 'enterprise',
    //                 enterprise: element.enterprise
    //             };

    //             this.api.addDocument(element).then(data=>{
    //                 console.log('ok');
                    
    //             })
    //         }
    //     });

    // }



}