import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditPersonPage } from './edit-person.page';
import { SharedComponentsModule } from 'src/app/components/shared-components.module';
import { ImageUploadModule } from "angular2-image-upload";
const routes: Routes = [
  {
    path: '',
    component: EditPersonPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ImageUploadModule.forRoot(),
    SharedComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditPersonPage]
})
export class EditPersonPageModule {}
