import { Component, OnInit, NgZone } from '@angular/core';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'app-edit-person',
    templateUrl: './edit-person.page.html',
    styleUrls: ['./edit-person.page.scss'],
})
export class EditPersonPage implements OnInit {

    personName: string;
    person: any;
    jobsData: boolean = false;
    person_key: any;
    jobs: any = [];

    experiences: any = [];
    starterForm: FormGroup;
    interests = {};
    CVFile: any = null;
    programs: any;

    constructor(
        private route: ActivatedRoute,
        public api: ApiService,
        private zone: NgZone,
        public loadingCtrl: LoadingController,
        public alertController: AlertController,
        public modalController: ModalController,
        public router: Router,
        public formBuilder: FormBuilder

    ) {

        this.starterForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            cv: ['', [Validators.required]],
            image: ['', [Validators.required]],
            lastname: ['', [Validators.required]],
            civil_status: [''],
            gender: ['', [Validators.required]],
            email: ['', [Validators.required]],
            address: [''],
            dui: [''],
            nit: [''],
            nup: [''],
            program: [''],
            experience: [''],
            studys: [''],
            careers: [''],
            courses: [''],
            references: [''],
        });

        this.api.getPrograms()
            .on('value', snapshots => {
                this.programs = [];
                snapshots.forEach(element => {
                    let program = element.val();
                    program.key = element.key;
                    this.programs.push(program);
                });

                this.forceUpdate();
            });

        this.personName = this.route.snapshot.paramMap.get('name');
        this.personName = this.personName.replace("_", " ")
        this.person_key = this.route.snapshot.paramMap.get('key');

        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.getApplicant(this.person_key)
                    .orderByChild('name')
                    .on('value', resp => {
                        let person = resp.val();
                        person.key = resp.key;

                        this.api.getPicture(person.key)
                            .on('value', Snapshot => {
                                person.image = Snapshot.val();
                                if (person.image == null) {
                                    person.image = {
                                        downloadURL: 'http://www.alempleo.org/demo/img/imageholder.jpg'
                                    }
                                }
                                this.person = person;
                                this.starterForm = this.formBuilder.group({
                                    name: [person.name, [Validators.required]],
                                    lastname: [person.lastname, [Validators.required]],
                                    cv: ['', [Validators.required]],
                                    image: ['', [Validators.required]],
                                    civil_status: [person.civil_status],
                                    gender: [person.gender, [Validators.required]],
                                    email: [person.email, [Validators.required]],
                                    address: [person.address],
                                    dui: [person.dui],
                                    nit: [person.nit],
                                    nup: [person.nup],
                                    program: [person.program],
                                    experience: [person.experience],
                                    studys: [person.studys],
                                    careers: [person.careers],
                                    courses: [person.courses],
                                    references: [person.references],
                                });

                                if (person.experience) {
                                    this.experiences = person.experience;
                                }
                                if (person.studys) {
                                    this.studys = person.studys;
                                }
                                if (person.careers) {
                                    this.careers = person.careers;
                                }
                                if (person.courses) {
                                    this.courses = person.courses;
                                }
                                if (person.references) {
                                    this.references = person.references;
                                }

                                loader.dismiss();
                            })
                    });
            });
        });
    }

    ngOnInit() {

    }

    async loader() {
        return await this.loadingCtrl.create({
            spinner: 'bubbles',
            animated: true,
            mode: 'ios',
            translucent: true,
            cssClass: 'custom-class custom-loading',
        });
    }

    // AddInterest(interest) {
    //     this.interests[interest.key] = interest;
    //     interest.added = true;
    //     this.thirdStepForm.controls['work_items'].setValue(this.interests);
    // }

    async addExperience() {
        const alert = await this.alertController.create({
            header: 'Agrega tu experiencia Laboral',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre de la empresa'
                },
                {
                    name: 'work_item',
                    type: 'text',
                    placeholder: 'Giro'
                },
                {
                    name: 'phone',
                    type: 'text',
                    placeholder: 'Telefono'
                },
                {
                    name: 'position',
                    type: 'text',
                    placeholder: 'Cargo desempeñado'
                },
                {
                    name: 'main_functions',
                    type: 'text',
                    placeholder: 'Principales funciones realizadas'
                },
                {
                    name: 'chief',
                    type: 'text',
                    placeholder: 'Nombre de Jefe inmediato'
                },
                {
                    name: 'chief_position',
                    type: 'text',
                    placeholder: 'Cargo de Jefe inmediato'
                },
                {
                    name: 'start',
                    type: 'date',
                    placeholder: 'Fecha de ingreso'
                },
                {
                    name: 'end',
                    type: 'date',
                    placeholder: 'Fecha de salida'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.experiences.push(data);
                        this.starterForm.controls['experience'].setValue(this.experiences);
                    }
                }
            ]
        });

        await alert.present();
    }

    RemoveExperience(experience, index) {
        this.experiences.splice(index, 1);
        if (this.isEmpty(this.experiences)) {
            this.starterForm.controls['experience'].setValue('');
        } else {
            this.starterForm.controls['experience'].setValue(this.experiences);
        }
    }

    careers = [];

    async addCareer() {
        const alert = await this.alertController.create({
            header: 'Agregar estudio superior',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Carrera'
                },
                {
                    name: 'institution',
                    type: 'text',
                    placeholder: 'Institución'
                },
                {
                    name: 'level',
                    type: 'text',
                    placeholder: 'Nivel alcanzado'
                },
                {
                    name: 'period',
                    type: 'text',
                    placeholder: 'Periodo'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.careers.push(data);
                        this.starterForm.controls['careers'].setValue(this.careers);
                    }
                }
            ]
        });
        await alert.present();
    }

    RemoveCareer(career, index) {
        this.careers.splice(index, 1);
        if (this.isEmpty(this.careers)) {
            this.starterForm.controls['careers'].setValue('');
        } else {
            this.starterForm.controls['careers'].setValue(this.careers);
        }
    }

    studys: any = [];

    async addStudy() {
        const alert = await this.alertController.create({
            header: 'Agregar nivel de estudio',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'institution',
                    type: 'text',
                    placeholder: 'Institución'
                },
                {
                    name: 'period',
                    type: 'text',
                    placeholder: 'Periodo'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.studys.push(data);
                        this.starterForm.controls['studys'].setValue(this.studys);
                    }
                }
            ]
        });
        await alert.present();
    }

    RemoveStudy(study, index) {
        this.studys.splice(index, 1);
        if (this.isEmpty(this.studys)) {
            this.starterForm.controls['studys'].setValue('');
        } else {
            this.starterForm.controls['studys'].setValue(this.studys);
        }
    }

    courses: any = [];

    async addCourse() {
        const alert = await this.alertController.create({
            header: 'Agregar curso o seminarios',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre'
                },
                {
                    name: 'institution',
                    type: 'text',
                    placeholder: 'Institución'
                },
                {
                    name: 'duration',
                    type: 'text',
                    placeholder: 'Duración'
                },
                {
                    name: 'date',
                    type: 'date',
                    placeholder: 'Fecha'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.courses.push(data);
                        this.starterForm.controls['courses'].setValue(this.courses);
                    }
                }
            ]
        });
        await alert.present();
    }

    RemoveCourse(course, index) {
        this.courses.splice(index, 1);
        if (this.isEmpty(this.courses)) {
            this.starterForm.controls['courses'].setValue('');
        } else {
            this.starterForm.controls['courses'].setValue(this.courses);
        }
    }

    references: any = [];

    async addReference() {
        const alert = await this.alertController.create({
            header: 'Agregar una referencia',
            cssClass: 'experience-alert',
            inputs: [
                {
                    name: 'name',
                    type: 'text',
                    placeholder: 'Nombre'
                },
                {
                    name: 'enterprise',
                    type: 'text',
                    placeholder: 'Empresa'
                },
                {
                    name: 'position',
                    type: 'text',
                    placeholder: 'Cargo'
                },
                {
                    name: 'phone',
                    type: 'text',
                    placeholder: 'Telefono'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: 'Agregar',
                    handler: (data) => {
                        this.references.push(data);
                        this.starterForm.controls['references'].setValue(this.references);
                    }
                }
            ]
        });
        await alert.present();
    }

    RemoveReference(course, index) {
        this.references.splice(index, 1);
        if (this.isEmpty(this.references)) {
            this.starterForm.controls['references'].setValue('');
        } else {
            this.starterForm.controls['references'].setValue(this.references);
        }
    }

    isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }

    updateProfile() {
        this.loader().then(loader => {
            loader.present().then(() => {
                this.api.updatePersonProfile(this.person.key, this.starterForm.value, this.CVFile).then(data => {
                    loader.dismiss();
                    this.confirmAdded();
                });
            })
        })
    }

    async confirmAdded() {
        const alert = await this.alertController.create({
            header: 'Datos actualizados!',
            message: 'Tus datos han sido actualizados correctamente.',
            buttons: ['Aceptar']
        });

        await alert.present();
    }


    upload(event) {
        this.CVFile = event.target.files[0]
        this.starterForm.controls['cv'].setValue('ya');
    }

    finish() {
        this.router.navigateByUrl('/dashboard/personas', { replaceUrl: true });
    }

    onUploadFinished(file) {
        this.starterForm.controls['image'].setValue(file.src);
    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }
    

}
