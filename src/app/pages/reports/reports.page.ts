import { Component, OnInit, NgZone } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { ModalController } from '@ionic/angular';
import { ApplicantComponent } from 'src/app/components/applicant/applicant.component';
import { Angular5Csv } from 'angular5-csv/dist/Angular5-csv';
import { Router } from '@angular/router';

@Component({
    selector: 'app-reports',
    templateUrl: './reports.page.html',
    styleUrls: ['./reports.page.scss'],
})
export class ReportsPage implements OnInit {

    plazasData: any;
    OpenplazasCategories: any;
    ClosedplazasCategories: any;
    jobs: any;
    all_jobs: any = [];
    open_jobs: any = [];
    closed_jobs: any = [];

    constructor(
        public api: ApiService,
        private zone: NgZone,
        public modalController: ModalController,
        public router: Router
    ) {
        this.api.getMember()
            .on('value', Snapshot => {
                if (Snapshot.val().rol == 'supervisor') {
                    this.router.navigate(['/dashboard'])
                } else {
                    this.init();
                }
            });
    }

    init() {
        this.api.getJobs()
            .orderByChild('status')
            .equalTo('cerrada')
            .once('value', snapshot => {
                this.jobs = [];
                snapshot.forEach(childSnapshot => {
                    let item = childSnapshot.val();
                    item.key = childSnapshot.key;
                    item.name_url = item.position.replace(" ", "_");
                    item.enterprise.name_url = item.enterprise.name.replace(" ", "_");

                    this.api.getContratation()
                        .orderByChild('job/key')
                        .equalTo(item.key)
                        .on('value', Snapshots => {
                            Snapshots.forEach(element => {
                                let contratation = element.val();
                                contratation.key = element.key;
                                item.contratation = contratation;
                            });
                            this.jobs.push(item);
                            this.all_jobs.push(item);
                            this.forceUpdate();
                        })
                });
                this.forceUpdate();
            });

        this.plazasData = {
            chartType: 'PieChart',
            dataTable: [
                ['Estados', 'Percent']
            ]
        };

        this.OpenplazasCategories = {
            chartType: 'PieChart',
            dataTable: [
                ['Categorias', 'Percent']
            ]
        };

        this.ClosedplazasCategories = {
            chartType: 'PieChart',
            dataTable: [
                ['Categorias', 'Percent']
            ]
        };

        this.api.getJobs()
            .once('value', Snapshots => {
                let abiertas = 0;
                let cerradas = 0;
                let categories_opens = [];
                let categories_closed = [];

                this.plazasData.dataTable = [
                    ['Estados', 'Percent']
                ]

                this.OpenplazasCategories.dataTable = [
                    ['Categorias', 'Percent']
                ]

                Snapshots.forEach(element => {
                    let job = element.val();
                    job.key = element.key;
                    this.all_jobs.push(job);
                    if (job.status == 'abierta') {
                        abiertas += 1;
                        this.open_jobs.push(job);
                        let category = job.kind_job.name;
                        let find_category = this.search(category, categories_opens);
                        if (find_category) {
                            find_category.number = find_category.number + 1;
                        } else {
                            categories_opens.push({ name: category, number: 1 });
                        }
                    } else {
                        cerradas += 1;
                        this.closed_jobs.push(job);
                        let category = job.kind_job.name;
                        let find_category = this.search(category, categories_closed);
                        if (find_category) {
                            find_category.number = find_category.number + 1;
                        } else {
                            categories_closed.push({ name: category, number: 1 });
                        }
                    }

                });

                this.plazasData.dataTable.push(['abiertas', abiertas], ['cerradas', cerradas]);
                if (this.plazasData.component) {
                    this.plazasData.component.draw();
                }

                categories_opens.forEach(element => {
                    this.OpenplazasCategories.dataTable.push([element.name, element.number]);
                });
                if (this.OpenplazasCategories.component) {
                    this.OpenplazasCategories.component.draw();
                }

                categories_closed.forEach(element => {
                    this.ClosedplazasCategories.dataTable.push([element.name, element.number]);
                });
                if (this.ClosedplazasCategories.component) {
                    this.ClosedplazasCategories.component.draw();
                }
            });
    }

    ngOnInit() {


    }

    forceUpdate() {
        this.zone.run(() => {
            console.log('force update the screen');
        });
    }

    search(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].name === nameKey) {
                return myArray[i];
            }
        }
    }

    async previewApplicant(applicant) {

        const modal = await this.modalController.create({
            component: ApplicantComponent,
            componentProps: {
                applicant: applicant
            }
        });
        return await modal.present();
    }

    plazasDataReport() {

        var data = [

        ];

        this.open_jobs.forEach(element => {
            data.push({
                Plaza: element.position,
                Salario: element.salary,
                tipo: element.type,
                Categoria: element.kind_job.name,
                gender: element.gender,
                experience: element.experience,
                empresa: element.enterprise.name,
                email: element.enterprise.email,
                number: element.enterprise.phone,
                sucursal: element.branch.name,
            });
        });

        let options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            title: 'Reporte de plazas activas',
            useBom: true,
            headers: ["Plaza", "Salario", "Tipo de trabajo", "Categoria", "Genero requerido", "Experiencia requerida", "Empresa", "Correo de empresa", "Numero de empresa", "Sucursal"],
            nullToEmptyString: true,
        };

        new Angular5Csv(data, 'Plazas Activas', options);
    }

    plazasCerradasReport() {
        var data = [

        ];

        console.log(this.closed_jobs);

        this.closed_jobs.forEach(element => {
            data.push({
                Plaza: element.position,
                Salario: element.salary,
                tipo: element.type,
                Categoria: element.kind_job.name,
                gender: element.gender,
                experience: element.experience,
                empresa: element.enterprise.name,
                email: element.enterprise.email,
                number: element.enterprise.phone,
                sucursal: element.branch.name,
            });
        });

        let options = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalseparator: '.',
            showLabels: true,
            title: 'Reporte de plazas Cerradas',
            useBom: true,
            headers: ["Plaza", "Salario", "Tipo de trabajo", "Categoria", "Genero requerido", "Experiencia requerida", "Empresa", "Correo de empresa", "Numero de empresa", "Sucursal"],
            nullToEmptyString: true,
        };

        new Angular5Csv(data, 'Plazas Cerradas', options);
    }

}
