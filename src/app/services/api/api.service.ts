import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { AuthService } from '../auth/auth.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import 'firebase/storage';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(
        private angularAuth: AngularFireAuth,
        public database: AngularFireDatabase,
        private auth: AuthService,
        public http: Http
    ) { }

    getWorkItems() {
        return firebase.database().ref(`work-items/`);
    }

    getApplicants() {
        return firebase.database().ref(`applicants/`);
    }

    getApplications() {
        return firebase.database().ref(`applications/`);
    }

    getApplicant(key) {
        return firebase.database().ref(`applicants/${key}`);
    }

    getEnterprise(key) {
        return firebase.database().ref(`enterprises/${key}`);
    }

    getEnterprises() {
        return firebase.database().ref(`enterprises/`);
    }

    getJobs() {
        return firebase.database().ref(`jobs/`);
    }

    getContratation() {
        return firebase.database().ref(`contratations/`);
    }

    getPeople() {
        return firebase.database().ref(`applicants/`);
    }

    getJob(key) {
        return firebase.database().ref(`jobs/${key}`);
    }

    getPrograms() {
        return firebase.database().ref(`programs`);
    }

    getMembers() {
        return firebase.database().ref(`admins`);
    }

    getMember() {
        return firebase.database().ref(`admins/${this.auth.token}`);
    }

    get(document){
        return firebase.database().ref(`${document}`);
    }

    IsApplicant() {
        return new Promise((resolve, reject) => {
            // firebase.database().ref(`applicants/${this.auth.token}/`)
            //     .on('value', Snapshot => {
            //         let user = Snapshot.val();
            //         if (user) {
            //             resolve(true);
            //         } else {
            //             resolve(false);
            //         }
            //     })
        });
    }

    activeEnterprise(key) {
        return new Promise((resolve, reject) => {
            let updates = {};
            updates[`/enterprises/${key}/status`] = 'active';
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    activeApplicants(key) {
        return new Promise((resolve, reject) => {
            let updates = {};
            updates[`/applicants/${key}/status`] = 'active';
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    updateAdmin(key, type) {
        return new Promise((resolve, reject) => {
            let updates = {};
            updates[`/admins/${key}/rol`] = type;
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    removeProgram(key) {
        return new Promise((resolve, reject) => {
            firebase.database().ref(`programs/${key}`).remove(data => {
                resolve(data);
            });
        });
    }

    addProgram(data) {
        let program_key = firebase.database().ref().child('programs').push().key;
        data.date = this.GetTodayIsoFormat(new Date());

        return new Promise((resolve, reject) => {
            let updates = {};
            updates[`/programs/${program_key}`] = data;
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }



    addJob(data, enterprise) {
        let job_key = firebase.database().ref().child('jobs').push().key;
        data.date = this.GetTodayIsoFormat(new Date());
        return new Promise((resolve, reject) => {
            let updates = {};
            data.opened_by = 'Alempleo';
            data.status = 'abierta';
            updates[`/jobs/${job_key}`] = data;
            updates[`/enterprise-jobs/${enterprise.key}/${job_key}`] = data;
            updates[`/kind-jobs/${data.kind_job.key}/${job_key}`] = data;
            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    GetJobApplicants(job_key) {
        return firebase.database().ref(`applications-jobs/${job_key}/`);
    }

    GetTodayIsoFormat(date) {
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let dt = date.getDate();
        let dt2, month2;

        if (dt < 10) {
            dt2 = '0' + dt;
        } else {
            dt2 = dt;
        }
        if (month < 10) {
            month2 = '0' + month;
        } else {
            month2 = month;
        }

        return `${year}-${month2}-${dt2}`;
    }

    applyToAJob(job, applicant) {
        return new Promise((resolve, reject) => {
            let updates = {};
            applicant.applicant_key = applicant.key;

            let application_key = firebase.database().ref().child('applications').push().key;
            let notification_key = firebase.database().ref().child('notifications').push().key;

            let data = {
                applicant: applicant,
                job: job,
                alempleo: false,
                date: this.GetTodayIsoFormat(new Date())
            };

            let notification_data = {
                to: applicant.key,
                date: this.GetTodayIsoFormat(new Date()),
                from: job.enterprise.name,
                reason: 'recomendation',
                job: job,
                applicant: applicant,
                new: true
            }

            applicant.alempleo = true;
            updates[`/applications-jobs/${job.key}/${application_key}`] = applicant;
            updates[`/applications/${application_key}`] = data;
            updates[`/notifications/${applicant.key}/${notification_key}`] = notification_data;
            updates[`/jobs/${job.key}/tracing`] = this.auth.token;

            firebase.database().ref().update(updates).then(data => {
                resolve(data);
            }, (err) => {
                reject(err);
            });
        });
    }

    getPicture(key) {
        return firebase.database().ref(`/images/${key}`);
    }

    randomPassword(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }


    createEnterprise(data) {
        let self = this;

        return new Promise((resolve, reject) => {
            let password = Math.random().toString(36).slice(-6);
            password = password.toUpperCase();

            let user_key = firebase.database().ref().child('enterprises').push().key;
            data.pswd = this.randomPassword(6);
            data.created_at = firebase.database.ServerValue.TIMESTAMP;
            data.last_login = firebase.database.ServerValue.TIMESTAMP;
            data.alempleo = true;

            if (data.image) {
                this.uploadImage(user_key, data.image).then(imageData => {
                    data.image = imageData;
                    this.addEnterprise(data, resolve, reject, user_key);
                });
            } else {
                this.addEnterprise(data, resolve, reject, user_key);
            }
        });
    }

    addEnterprise(data, resolve, reject, user_key) {
        let updates = {};
        let self = this;

        let enterprise_key = firebase.database().ref().child('users').push().key;

        let enterprise = {
            key: enterprise_key,
            name: data.enterprise,
            branches: data.branches,
            email_contact: data.email_contact,
            phone_contact: data.phone_contact,
            name_contact: data.name,
            lastname_contact: data.lastname,
            status: 'active'
        }

        let user_enterprise = {
            alempleo: true,
            name: data.name,
            lastname: data.lastname,
            branches: data.lastname,
            email: data.email,
            enterprise: enterprise,
            type: 'enterprise',
            created_at: firebase.database.ServerValue.TIMESTAMP,
            last_login: firebase.database.ServerValue.TIMESTAMP,
        }

        updates[`/enterprises/${enterprise_key}`] = enterprise;
        updates[`/user-enterprises/${user_key}`] = user_enterprise;
        updates[`/images/${enterprise_key}/`] = data.image;

        firebase.database().ref().update(updates).then(response => {
            let body = new FormData();
            body.append('name', data.name);
            body.append('email', data.email);
            body.append('pswd', data.pswd);
            self.http.get('http://www.alempleo.org/mail/mail_user_creation.php?email=' + data.email + '&name=' + data.name + '&pswd=' + data.pswd)
                .subscribe(data => {
                    resolve(data);
                });
        }, (err) => {
            reject(err);
        });
    }

    updateEnterprise(data, enterprise_key) {

        return new Promise((resolve, reject) => {
            let enterprise = {
                name: data.enterprise,
                branches: data.branches,
                email_contact: data.email_contact,
                phone_contact: data.phone_contact,
                name_contact: data.phone_contact,
                lastname_contact: data.phone_contact,
            }

            let updates = {};


            if (data.image) {
                this.uploadImage(enterprise_key, data.image).then(imageData => {
                    this.database.database.ref(`/images/${enterprise_key}/`).update(imageData).then(rsesult => {
                        this.database.database.ref(`/enterprises/${enterprise_key}/`).update(enterprise).then(data => {
                            resolve(data);
                        }, err => {
                            reject(err);
                        })
                    }, err => {
                        reject(err);
                    })
                });
            } else {
                this.database.database.ref(`/enterprises/${enterprise_key}/`).update(enterprise).then(data => {
                    resolve(data);
                }, err => {
                    reject(err);
                })
            }
        })
    }

    createPerson(applicant_data, CVFile) {
        let self = this;

        return new Promise((resolve, reject) => {

            let user_key = firebase.database().ref().child('users').push().key;
            applicant_data.pswd = this.randomPassword(6);
            applicant_data.created_at = firebase.database.ServerValue.TIMESTAMP;
            applicant_data.last_login = firebase.database.ServerValue.TIMESTAMP;
            applicant_data.alempleo = true;

            if (applicant_data.image) {
                this.uploadImage(user_key, applicant_data.image).then(imageData => {
                    applicant_data.image = imageData;
                    self.addCV(applicant_data, resolve, reject, CVFile, user_key);
                });
            } else {
                self.addCV(applicant_data, resolve, reject, CVFile, user_key);
            }
        });
    }

    addCV(starter_data, resolve, reject, CVFile, user_key) {
        let self = this;
        if (CVFile) {
            let storageRef = firebase.storage().ref();
            const filename = this.generateUUID();
            let fileRef = storageRef.child(`cv/${user_key}/${filename}`);
            fileRef.put(CVFile).then(data => {
                fileRef.getDownloadURL().then(function (downloadURL) {
                    console.log('si hay CV');

                    starter_data.cv = downloadURL;
                    self.afterStart(starter_data, resolve, reject, user_key)
                });
            });
        } else {
            self.afterStart(starter_data, resolve, reject, user_key)
        }
    }

    afterStart(starter_data, resolve, reject, user_key) {
        let updates = {};
        let self = this;
        starter_data.status = 'active';
        updates[`/applicants/${user_key}`] = starter_data;
        updates[`/images/${user_key}`] = starter_data.image;
        firebase.database().ref().update(updates).then(data => {
            firebase.database().ref().update(updates).then(response => {
                let body = new FormData();
                body.append('name', starter_data.name);
                body.append('email', starter_data.email);
                body.append('pswd', starter_data.pswd);
                self.http.get('http://www.alempleo.org/mail/mail_user_creation.php?email=' + starter_data.email + '&name=' + starter_data.name + '&pswd=' + starter_data.pswd)
                    .subscribe(data => {
                        resolve(data);
                    });
            }, (err) => {
                reject(err);
            });
        }, (err) => {
            reject(err);
        });
    }

    uploadImage(user_key, image) {
        return new Promise((resolve, reject) => {

            let storageRef = firebase.storage().ref();
            const filename = this.generateUUID();
            let imageRef = storageRef.child(`images/${filename}.jpg`);

            imageRef.putString(image, firebase.storage.StringFormat.DATA_URL)
                .then(data => {
                    let photoData = {};
                    photoData['fullPath'] = data.metadata.fullPath;
                    photoData['size'] = data.metadata.size;
                    photoData['contentType'] = data.metadata.contentType;
                    photoData['md5Hash'] = data.metadata.md5Hash;
                    photoData['bucket'] = data.metadata.bucket;
                    photoData['updated'] = data.metadata.updated;
                    imageRef.getDownloadURL().then(data => {
                        photoData['downloadURL'] = data;
                        resolve(photoData);
                    });
                })
        })
    }

    private generateUUID(): string {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    updatePersonProfile(key, data, cv) {
        let self = this;

        return new Promise((resolve, reject) => {
            if (data.image) {
                this.uploadImage(key, data.image).then(imageData => {
                    this.database.database.ref(`/images/${key}/`).update(imageData).then(rsesult => {
                        self.updateCV(data, resolve, reject, cv, key);
                    }, err => {
                        reject(err);
                    })
                });
            } else {
                self.updateCV(data, resolve, reject, cv, key);
            }
        });
    }

    updateCV(starter_data, resolve, reject, CVFile, user_key) {
        let self = this;
        console.log(CVFile);

        if (CVFile) {
            let storageRef = firebase.storage().ref();
            const filename = this.generateUUID();
            let fileRef = storageRef.child(`cv/${user_key}/${filename}`);
            fileRef.put(CVFile).then(data => {
                fileRef.getDownloadURL().then(function (downloadURL) {
                    starter_data.cv = downloadURL;
                    self.updateFinal(starter_data, resolve, reject, user_key)
                });
            });
        } else {
            delete starter_data.cv;
            self.updateFinal(starter_data, resolve, reject, user_key)
        }
    }

    updateFinal(starter_data, resolve, reject, user_key) {

        this.database.database.ref(`/applicants/${user_key}/`).update(starter_data).then(data => {
            resolve(data);
        }, err => {
            reject(err);
        })
    }


    addDocument(data) {
        return new Promise((resolve, reject) => {
            let user_key = firebase.database().ref().child('enterprises').push().key;

            let updates = {};
            let self = this;

            let enterprise_key = firebase.database().ref().child('users').push().key;

            let enterprise = {
                key: enterprise_key,
                name: data.enterprise,
                email_contact: data.email,
                name_contact: data.name,
                status: 'active'
            }

            let user_enterprise = {
                alempleo: true,
                name: data.name,
                email: data.email,
                enterprise: enterprise,
                type: 'enterprise',
                created_at: firebase.database.ServerValue.TIMESTAMP,
                last_login: firebase.database.ServerValue.TIMESTAMP,
            }

            updates[`/enterprises/${enterprise_key}`] = enterprise;
            updates[`/user-enterprises/${user_key}`] = user_enterprise;

            firebase.database().ref().update(updates).then(response => {
                resolve(true);
            }, (err) => {
                reject(err);
            });

        });
    }
}